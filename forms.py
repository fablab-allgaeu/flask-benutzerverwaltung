from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo

class UpdateUserDataForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    rfid_tag = StringField('Rfid Tag')
    name = StringField("Full Name")
    street = StringField("Steetname and number")
    city = StringField("Postcode and cityname")
    submit = SubmitField('Update')


class AddNewRoleForm(FlaskForm):
    role = StringField('Role')
    submit = SubmitField('Add role')
