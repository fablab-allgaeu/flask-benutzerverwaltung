#!/usr/bin/env python3

import sys
from flask import Flask, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_material import Material
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, roles_required
from flask_security.utils import encrypt_password
from flask_migrate import Migrate
from sqlalchemy import event
import re
import json
from config_prod import Config
import datetime

user_datastore = None
security = None

db = SQLAlchemy()

def create_our_app(config):
    app = Flask(__name__)
    app.secret_key = "secret key"
    app.config.from_object(config)
    app.config['WTF_CSFR_ENABLED'] = False
    Material(app)
    db.init_app(app)
    migrate = Migrate(app, db)

    # Setup Flask-Security
    global security
    security = Security(app, user_datastore)
    assert security

    register_blueprints(app)

    return app

def get_user_datastore():
    global user_datastore
    assert user_datastore
    return user_datastore

def register_blueprints(app):
    from routes import route_blueprint
    app.register_blueprint(route_blueprint)


# Define models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class Usage(db.Model):
    __tablename__ = 'usage'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(), server_default=db.func.current_timestamp())
    type = db.Column(db.String(100))
    duration = db.Column(db.Integer())
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    user = db.relationship('User', back_populates='usages')

    def getUser(self):
        return get_user_datastore().get_user(self.user_id)


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean(), default=False)
    confirmed_at = db.Column(db.DateTime())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(100))
    current_login_ip = db.Column(db.String(100))
    rfid_tag = db.Column(db.String(11), default="00:00:00:00")
    login_count = db.Column(db.BigInteger())
    name = db.Column(db.String(255), default="")
    street = db.Column(db.String(255), default="")
    city = db.Column(db.String(255), default="")
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    usages = db.relationship('Usage', back_populates='user')

    def __str__(self):
        return "<User: %s>" % self.email

user_datastore = SQLAlchemyUserDatastore(db, User, Role)

@event.listens_for(User.email, 'set')
def user_check_email(target, value, old_value, initiater):
    email = value
    assert email is not None, "email is empty"
    assert len(email) > 4, "email is too short"
    assert re.match("[^@]+@[^@]+\.[^@]+", email), "not a email"
    user = User.query.filter_by(email=email).first()
    if user:
        assert user.id == target.id, "email already in usage from an other user"

@event.listens_for(User.rfid_tag, 'set', retval=True)
def user_check_rfid_tag(target, value, old_value, initiater):
    rfid_tag = value.upper()
    assert rfid_tag is not None, "rfid_tag is empty"
    assert len(rfid_tag) == 11 , "rfid size(%s) does not match" %  rfid_tag
    assert re.match("[0-9A-F]+:[0-9A-F]+:[0-9A-F]+:[0-9A-F]+", rfid_tag), "syntax not correct"
    user = User.query.filter_by(rfid_tag=rfid_tag).first()
    if user:
        assert user.id == target.id, "rfid tag already in usage from an other user"
    return rfid_tag # write because of upper()
    


def create_default_user(db):
    db.create_all()
    user_datastore.find_or_create_role('admin')
    user = User.query.filter_by(email='admin@admin.admin').first()
    if user is None:
        new_admin = user_datastore.create_user(email='admin@admin.admin', password='password')
        user_datastore.add_role_to_user( new_admin, 'admin')
        user = new_admin
    user.active = True # set always on, so we cannot lock out
    db.session.commit()


if __name__ == '__main__':
    app = create_our_app(Config)

    @app.before_first_request
    def init_user():
        create_default_user(db)

    app.run()

