from flask_security import login_required, roles_required
from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask import abort, jsonify
from main import User, Role, get_user_datastore, db, Usage
from forms import UpdateUserDataForm, AddNewRoleForm
from flask_login import current_user

route_blueprint = Blueprint('recipes',__name__)

# Views

import sys

@route_blueprint.route('/')
@login_required
def home():
    return render_template('index.html', title="Welcome", user=current_user)

@route_blueprint.route('/admin')
@roles_required('admin')
def admin():
    return render_template('admin.html', title="Admin Area")

@route_blueprint.route('/admin/users')
@roles_required('admin')
def users():
    return render_template('users.html', users=User.query.all(), title="Users")

@route_blueprint.route('/admin/modify_user', methods=['GET', 'POST'])
@roles_required('admin')
def modify_user():
    arg_email = request.args.get('email')
    user = get_user_datastore().find_user(email=arg_email)
    if not user:
        flash("Invalid User %s" % arg_email)
        return redirect(url_for('recipes.users'))
    form = UpdateUserDataForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                user.email = form.email.data
                if form.rfid_tag.data:
                    user.rfid_tag = form.rfid_tag.data
                else:
                    user.rfid_tag = None
              #  user.active = form.active.data
              #  user.confirmed = form.confirmed.data
                user.name = form.name.data
                user.street = form.street.data
                user.city = form.city.data
                db.session.commit()

                flash("Updated user %s" % form.rfid_tag.data)
            except:
                flash("Error setting new data for %s" % user.email)
            return redirect(url_for('recipes.users'))
        else:
            app.logger.debug(form.errors)
            flash("Error updating for %s:%s" % ( user.email, form.errors) )
            return redirect(url_for('recipes.users'))
    else:
        return render_template('modify_user.html', title="Modify User", user=user, form=form)

@route_blueprint.route('/admin/roles',methods=['GET', 'POST'])
@roles_required('admin')
def roles():
    form = AddNewRoleForm()
    if request.method == "POST":
        if form.validate_on_submit():
            try:
                role_name = form.role.data
                assert not Role.query.filter_by(name=role_name).first(), "role already exists"
                get_user_datastore().find_or_create_role(role_name)
                db.session.commit()

                flash("Created role %s" % role_name)
            except Exception as e:
                flash("Error creation new role %s: %s" % (role_name, str(e) ))
    return render_template('roles.html', title="Roles", form=form, User=User, Role=Role)

@route_blueprint.route('/admin/role-delete')
@roles_required('admin')
def role_delete():
    arg_role_name = request.args.get('role')
    role = get_user_datastore().find_role(arg_role_name)
    if not role:
        flash("Invalid role: %s" % arg_role_name)
    if request.method == 'GET':
        arg_confirm = request.args.get('confirmed')
        if not arg_confirm: 
            return render_template('confirm.html',
                                   title='Delete role',
                                   message=('Delete role "%s"' % role.name),
                                   url_yes=url_for('recipes.role_delete',
                                       role=arg_role_name,
                                       confirmed='yes'),
                                   url_no=url_for('recipes.roles'))
        elif arg_confirm == 'yes':
            db.session.delete(role)
            db.session.commit()
            flash("Role %s deleted" % arg_role_name)
        else:
            flash("Invalid confirm arg: %s" % arg_confirm)
    elif request.method == "POST":
        if form.validate_on_submit():
            try:
                role_name = form.role.name
                assert not Role.query.filter_by(name=role_name).first(), "role already exists"
                get_user_datastore().find_or_create_role(role_name)
                db.session.commit()

                flash("Created role %s" % role_name)
            except Exception as e:
                flash("Error creation new role %s" % (role_name, str(e) ))
        
    return redirect(url_for('recipes.roles'))

@route_blueprint.route('/admin/role-user-add')
@roles_required('admin')
def role_add_user():
    arg_role_name = request.args.get('role')
    arg_user_mail = request.args.get('user')
  
    if not arg_role_name or not arg_user_mail:
        flash("no role or user arg")
        return redirect(url_for("recipes.roles"))
        
    role = get_user_datastore().find_role(arg_role_name)
    user = get_user_datastore().get_user(arg_user_mail)

    if not role or not user:
        flash("Invalid role or user")
        return redirect(url_for("recipes.roles"))

    if user.has_role(role):
        flash("User aleady has role")
        return redirect(url_for("recipes.roles"))

    get_user_datastore().add_role_to_user(user, role.name)
    db.session.commit()
    flash("added user(%s) to role(%s)" %(user.email, role.name))
    return redirect(url_for("recipes.roles"))


@route_blueprint.route('/admin/role-user-rm')
@roles_required('admin')
def role_rm_user():
    arg_role_name = request.args.get('role')
    arg_user_mail = request.args.get('user')
  
    if not arg_role_name or not arg_user_mail:
        flash("no role or user arg")
        return redirect(url_for("recipes.roles"))
        
    role = get_user_datastore().find_role(arg_role_name)
    user = get_user_datastore().get_user(arg_user_mail)

    if not role or not user:
        flash("Invalid role or user")
        return redirect(url_for("recipes.roles"))

    if not user.has_role(role):
        flash("User aleady has not the role")
        return redirect(url_for("recipes.roles"))

    get_user_datastore().remove_role_from_user(user, role.name)
    db.session.commit()
    flash("removed user(%s) from role(%s)" %(user.email, role.name))
    return redirect(url_for("recipes.roles"))

@route_blueprint.route('/rfid-has-role')
def rfid_has_role():
    rfidtag = request.args.get('rfidtag')
    arg_role = request.args.get('role')

    assert rfidtag, "rfidtag arg is empty"
    assert arg_role, "role arg is empty"

    user = get_user_datastore().find_user(rfid_tag=rfidtag)
    role = get_user_datastore().find_role(arg_role)

    assert user, "could not find user"
    assert user.name, "user name is empty"
    assert user.street, "user street is empty"
    assert user.city, "user city is empty"

    assert role, "could not find role"

    assert user.has_role(role.name), "user has not the role"
    
    usage = Usage(type=role.name, duration=1, user_id=current_user.id)
    db.session.add(usage)
    user.usages.append(usage)
    db.session.commit() 

    return jsonify({ "User": user.email }),200

def render_pdf(html):
    from xhtml2pdf import pisa
    from io import BytesIO, StringIO
    pdf = BytesIO()
    pisa.CreatePDF(StringIO(html), pdf)
    resp = pdf.getvalue()
    pdf.close()
    return resp

from flask import make_response

@route_blueprint.route('/invoice')
@login_required
def invoice():
    usage_id = int(request.args.get('usage_id'))

    usage = Usage.query.get(usage_id)

    assert current_user.get_id() != usage.getUser().id

    import datetime
    html = render_template('invoice.html', usage=usage, date=datetime.datetime.now(), user=current_user)
    #return html
    pdf = render_pdf(html)

    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'

    return response
