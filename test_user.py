import pytest

from main import create_our_app, db, User, create_default_user, user_datastore
from config_test import Config

@pytest.fixture
def test_client():
    flask_app = create_our_app(Config)
    flask_app.config['WTF_CSRF_ENABLED'] = False
    flask_app.config['WTF_CSRF_METHODS'] = []
    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()

@pytest.fixture
def init_database():
    db.drop_all()
    db.create_all()
    yield db
  #  db.drop_all()

@pytest.fixture
def default_user():
    create_default_user(db)

def test_home_page(test_client):
    response = test_client.get("/")
    assert b"You should be redirected automatically to target URL" in response.data
    assert response.status_code == 302

def login(client, email, password):
    logout(client)
    r = client.post("/login",
                    data=dict(email=email, password=password),
                    follow_redirects=True)
    assert b'Invalid password' not in r.data
    response_is_ok(r)
    return r

def response_is_ok(response):
    assert response.status_code == 200

def logout(client):
    r = client.get('/logout', follow_redirects=True)
    response_is_ok(r)

def register(client, email, password):
    r= client.post("/register",
                   data=dict(email=email,
                             password=password,
                             password_confirm=password),
                   follow_redirects=True)
    response_is_ok(r)
    user = User.query.filter_by(email=email).first()
    assert user
    assert user.email == email
    assert user.password != password
    assert user.active
    assert user.rfid_tag == "00:00:00:00"
    return r

@pytest.mark.parametrize('email,password', [
    ("test@test,test","t"),
    ("blubber","testhallo"),
    ("blubber",""),
    ("","testhallo"),
    ("",""),
    ])
def test_register_fail(test_client, init_database, email, password):
    with pytest.raises(Exception):
        resp = register(test_client, email, password)

def register_and_login_user(client, email, password):
    register(client, email, password)
    login(client, email, password)

#def test_double_user_registration():
#    assert False

@pytest.mark.parametrize('email,password', [
    ("test@tt22tt.ttt","fancypassword"),
    ("test@test.test", "validPassWord")
    ])
def test_register_success(test_client, init_database, email, password):
    register_and_login_user(test_client, email, password)
    logout(test_client)

def test_invalid_login(test_client):
    response = login(test_client, "blubb23", "blubb")

    assert response.status_code == 200
    print(response.data)
    assert b"user does not exist" in response.data

def valid_user(client):
    email = "test@test.test"
    password = "blubber32"
    register_and_login_user(client, email, password)
    return (email, password)
    

def test_admin_home_no_access(test_client, init_database):
    email, password = valid_user(test_client)
    r = test_client.get("/admin")
    with pytest.raises(Exception):
        response_is_ok(r)

def default_admin(client):
    email =  "admin@admin.admin"
    login(client, "admin@admin.admin","password")
    user = User.query.filter_by(email=email).first()
    assert user
    assert user.email == email
    assert user.active
    assert user.rfid_tag == "00:00:00:00"
    admin_role = "admin"
    assert user.has_role(admin_role)
    return email

def get_and_check(client, url):
    r = client.get(url)
    response_is_ok(r)
    return r
 
def test_admin_has_access(test_client, init_database, default_user):
    admin = default_admin(test_client)
    get_and_check(test_client, "/admin")
    get_and_check(test_client, "/admin/users")
    get_and_check(test_client, "/admin/roles")


def test_admin_can_modify_user(test_client, init_database, default_user):
    email, password = valid_user(test_client)
    logout(test_client)
    admin = default_admin(test_client)

    r = test_client.get("/admin/modify_user", follow_redirects=True)
    print(r)
    assert r.args == "/admin/users"

    with pytest.raises(Exception):
        test_client.get("/admin/modify_user?email=blubber@blulbber.blubber")

    test_client.get("/admin/modify_user?email=%s" % email)
    
    

def test_new_roles_on_db(test_client, init_database, default_user):
    new_role="blubber"
    role = user_datastore.create_role(name=new_role)
    db.session.add(role)
    email, pw = valid_user(test_client)
    user = user_datastore.find_user(email=email)
    assert new_role not in user.roles

    user_datastore.add_role_to_user(role=role, user=user)
    assert new_role in user.roles
    db.session.commit()
    
def test_new_role_via_web(test_client, init_database, default_user):
    new_role="hummel"
    email = default_admin(test_client)
    test_client.post("/admin/roles",
                     data=dict(role=new_role))
    assert user_datastore.find_role(new_role)
    user = user_datastore.find_user(email=email)
    assert new_role not in user.roles

    r = test_client.post("/admin/user-add-role",
                         data=dict(user=email, role=new_role))
    assert r.status_code == 200
    assert new_role in user.roles

    r = test_client.post("/admin/user-remove-role",
                         data=dict(user=email, role=new_role))
    assert r.status_code == 200
    assert new_role not in user.roles
