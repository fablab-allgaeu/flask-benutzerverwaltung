
class Config(object):
    DEBUG = False
    SECRET_KEY = 'super-secret'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'
    SECURITY_REGISTERABLE = True
    SECURITY_PASSWORD_SALT = "dumbass"
    SECURITY_SEND_REGISTER_EMAIL = False
    SECURITY_RECOVERABLE = True
    SECURITY_TRACKABLE = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'example@gmail.com'
    MAIL_PASSWORD = 'password'
    MAIL_DEFAULT_SENDER = 'example@gmail.com'
